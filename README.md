# ics-ans-syslog-ng

Ansible playbook to install syslog-ng.

## Requirements

- ansible >= 2.4
- molecule >= 2.6

## License

BSD 2-clause
